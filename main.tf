provider "aws" {
  region = "us-east-1"
}

data "aws_availability_zones" "available"{} 

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter { 
  name   = "name"
  values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_security_group" "web" {
    name = "Dynamic Security Group"
  
     dynamic "ingress" {
       for_each = ["22", "80"]
       content {  
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
   }
     egress {
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = ["0.0.0.0/0"]
    }
 
    tags = {
      Name  = "Web access for Application"
    }
  }

resource "aws_launch_configuration" "web" {
    name_prefix     = "Web-server-"
    image_id        = data.aws_ami.ubuntu.id
    instance_type   = "t2.micro"
    security_groups = [aws_security_group.web.id]
    #iam_instance_profile = "AmazonEC2RoleForSSM"
    key_name = "klyuchi"
}


resource "aws_autoscaling_group" "web" {
 name                 = "ASG-${aws_launch_configuration.web.name}"
 launch_configuration = aws_launch_configuration.web.name
 min_size             = 1
 max_size             = 1
 min_elb_capacity     = 1
 health_check_type    = "EC2"
 # vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id]
 availability_zones = [data.aws_availability_zones.available.names[0]]
 load_balancers       = [aws_elb.web.name]
}

resource "aws_elb" "web" {
  name               = "WebServer-Highly-Available-ELB"
  security_groups    = [aws_security_group.web.id]
  availability_zones = [data.aws_availability_zones.available.names[0]] 
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
   }
#  health_check {
#     healthy_threshold   = 2
#    unhealthy_threshold = 2
#    timeout             = 3
#    target              = "HTTP:80/"
#    interval            = 10
#   }

  tags = {
    Name = "WebServer-Highly-Available-ELB"
   }
  depends_on            = [aws_launch_configuration.web]
 }

resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
 }

output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}
